const express = require("express");
const db = require("../db");

const router = express.Router();

//create Book(book_id, book_title, publisher_name, author_name) 

//1.get books
router.get("/all",(request,response)=>{
    
    const statement = `select * from Book`;

    const connection = db.openConnection();
    connection.query(statement,(error,data)=>{
        if(error){
            response.send(error)
        }else {
            response.send(data)
        }

        connection.end();
    })

})

//2.add book
router.post("/add",(request,response)=>{
    const{book_title, publisher_name, author_name}=request.body;

    const statement = `insert into Book (book_title, publisher_name, author_name)
                       values ('${book_title}','${publisher_name}','${author_name}')`;

    const connection = db.openConnection();
    connection.query(statement,(error,data)=>{
        if(error){
            response.send(error)
        }else {
            response.send(data)
        }

        connection.end();
    })

})

router.put("/update",(request,response)=>{
    const{book_id,publisher_name, author_name}=request.body;

    const statement = `update Book set publisher_name='${publisher_name}' && 
                       author_name='${author_name}' where book_id=${book_id}`;

    const connection = db.openConnection();
    connection.query(statement,(error,data)=>{
        if(error){
            response.send(error)
        }else {
            response.send(data)
        }

        connection.end();
    })

})

router.delete("/delete",(request,response)=>{
    const{book_id}=request.body;

    const statement = `delete from Book where book_id=${book_id}`;

    const connection = db.openConnection();
    connection.query(statement,(error,data)=>{
        if(error){
            response.send(error)
        }else {
            response.send(data)
        }

        connection.end();
    })

})

module.exports = router;

